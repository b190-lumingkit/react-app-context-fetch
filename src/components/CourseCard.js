import { Col, Card, Stack, Button } from "react-bootstrap";
import { useState, useEffect } from "react";

export default function CourseCard({courseProp})
{
    const {name, description, price} = courseProp;

/* 
    use the state hook for this component to be able to store its state
    States are used to keep track of information related to individual components
        SYNTAXT:
            const [getter, setter] = useState(initialGetterValue)
    the setCount is asynchronous
*/
    const [count, setCount] = useState(0);  
    const [seats, setSeat] = useState(10);
    const [isOpen, setIsOpen] = useState(true)

    /* function enroll()
    {
        if (seat <= 0) return alert(`No more seats available`);
        if (seat > 0) return setCount(count + 1), setSeat(seat - 1);
    } */
    function enroll()
    {
        setCount(count + 1) 
        setSeat(seats - 1)
    }
    /* 
        effect hook - allows us to execute a piece of code whenever a component gets rendered or if a value of the state changes
        useEffect - requires two arguments: function and a dependancy array - BOTH ARE NEEDED
            function - to set which lines of codes are to be executed
            dependency array - identifies which variable/s are to be listened to, in terms of changing the state, for the function to be executed
                if the array is left empty, the codes will not listen to any variable but the codes will be executed once
    */
    useEffect(() => { if (seats === 0) return setIsOpen(false) }, [seats]);

    return (
        <Col className="p-3">
            <Card className="cardHighlight p-3">
                <Card.Body>
                    <Card.Title>
                        <h4>{name}</h4>
                    </Card.Title>
                    <Stack>
                        <div>
                            <h6>Description:</h6>
                            <p>{description}</p>
                        </div>
                        <div>
                            <h6>Price:</h6>
                            <p>Php {price}</p>
                        </div>
                    </Stack>
                    <Card.Text>Enrollees: {count} </Card.Text>
                    {   isOpen ?  
                        <Button variant={'primary'} onClick={enroll}>Enroll</Button>
                        :
                        <Button variant={'primary'} onClick={enroll} disabled>Enroll</Button>
                    }
                </Card.Body>
            </Card>
        </Col>
    )
}