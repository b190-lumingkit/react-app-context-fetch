import { Fragment, useContext, useEffect, useState} from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import { UserContext } from '../contexts/UserContext';

export default function AppNavbar() 
{
    /* 
        localStorage.getItem - used to get a piece of information from the browser's local storage. devs usually insert information in the local storage for specific purposes, one of commonly used purpose is for login
    */
    const user = useContext(UserContext),
        [isLogin, setIsLogin] = useState(false);

    useEffect(() => (user === null) ? setIsLogin(false) : setIsLogin(true), [user])
    
    return (
        <Navbar className="p-3" bg="light" expand="lg">
            <Navbar.Brand as={ Link } to ='/'>ZUITT</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"></Navbar.Toggle>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ml-auto">
                    <Nav.Link as={ NavLink } to ='/' exact='true'>Home</Nav.Link>
                    <Nav.Link as={ NavLink } to ='/courses' exact='true'>Courses</Nav.Link>
                    {   !isLogin ?  
                        <Fragment>
                            <Nav.Link as={ NavLink } to ='/login' exact='true'>Login</Nav.Link>
                            <Nav.Link as={ NavLink } to ='/register' exact='true'>Register</Nav.Link>
                        </Fragment>
                        :
                        <Nav.Link as={ NavLink } to ='/logout' exact='true'>Logout</Nav.Link>
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}