import { Button, Row, Col } from "react-bootstrap";
import { Fragment } from 'react';

export default function Banner(banner)
{
    return (
        <Row>
            <Col className="p-5">
                {
                    (banner.name !== 'error') ?
                    <Fragment>
                        <h1>Zuitt Coding Bootcamp</h1>
                        <p>Opportunities for everyone, everywhere</p>
                        <Button variant="primary">Enroll Now!</Button>
                    </Fragment>
                    :
                    <Fragment>
                        <h1>Page Not Found</h1>
                        <p>Go back to the <a className="text-decoration-none" href="/">homepage</a>.</p>
                    </Fragment>
                }
            </Col>
        </Row>
    )
}