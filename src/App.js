import './App.css';
import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

// Web Components
import AppNavbar from './components/AppNavbar';

// Web Pages
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';

// Contexts
import { UserContext } from './contexts/UserContext';

const user = localStorage.getItem('email');

/* 
  jsx are similar to HTML tags, with one major difference, that is being able to apply javascript code
*/

/* 
  Router (BrowserRouter) - used to wrap components that uses react-router-dom and allows the use of routes and the routing system

  Route - assigns an endpoint and displays the appropriate age component for that endpoint
    path - assigns the endpoint through string data type
*/

function App() {
  return (
    <UserContext.Provider value={user}>
        <Fragment>
        <Router>
          <AppNavbar />
          <Container fluid>
            <Routes>
              <Route path='/' element= {<Home />} />
              <Route path='/courses' element= {<Courses />} />
              <Route path='/register' element= {<Register />} />
              <Route path='/login' element= {<Login />} />
              <Route path='/logout' element= {<Logout />} />
              <Route path='*' element= {<Error />} />
            </Routes>
          </Container>
        </Router>
      </Fragment>
    </UserContext.Provider>
  );
}

export default App;
