import { Button, Form } from 'react-bootstrap';
import { useState, useEffect } from "react";

export default function Register()
{
    const [email, setEmail] = useState(""),
        [password1, setPassword1] = useState(""),
        [password2, setPassword2] = useState(""),
        [isActive, setIsActive] = useState(false);

    console.log(email)
    console.log(password1)    
    console.log(password2)

    useEffect(() => 
    { 
        if (email !== "" && password1 !== "" && password1 === password2)
        {
            setIsActive(true);
        }
        else
        {
            setIsActive(false);
        }
    }, [email, password1, password2]);

    function registerUser(e)
    {
        e.preventDefault();

        // clear input fields
        setEmail("");
        setPassword1("");
        setPassword2("");

        alert(`Registration successful.`);
    }

    return (
        <Form onSubmit={e => registerUser(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)} 
                    required 
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password1}
                    onChange={e => setPassword1(e.target.value)} 
                    required 
                />
            </Form.Group>
            <Form.Group className="mb-3" controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password2}
                    onChange={e => setPassword2(e.target.value)} 
                    required 
                />
            </Form.Group>
            {   isActive ?
                <Button variant="primary" type="submit" id="submitBtn">Register</Button>
                : 
                <Button variant="primary" type="submit" id="submitBtn" disabled>Register</Button>
            }  
        </Form>
    );
}