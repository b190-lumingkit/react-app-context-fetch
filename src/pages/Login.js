import { Button, Form } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom'
import { UserContext } from '../contexts/UserContext';

export default function Login()
{
    const [user, setUser] = useState(useContext(UserContext)),
        [email, setEmail] = useState(""),
        [password, setPassword] = useState(""),
        [isActive, setIsActive] = useState(false);

    useEffect(() => 
    { 
        (email !== "" && password !== "") ? setIsActive(true) : setIsActive(false) 
    }, [email, password]);

    function loginUser(e)
    {
        e.preventDefault();

        localStorage.setItem('email', email);
        window.dispatchEvent(new Event("storage"));
        setUser(email);

        setEmail("");
        setPassword("");
    }

    return (
        (user !== null) ?
        <Navigate to='/courses' />
        :
        <Form onSubmit={e => loginUser(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)} 
                    required 
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="userPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={e => setPassword(e.target.value)} 
                    required 
                />
            </Form.Group>
            {   isActive ?
                <Button variant="success" type="submit" id="submitBtn">Login</Button>
                : 
                <Button variant="success" type="submit" id="submitBtn" disabled>Login</Button>
            }  
        </Form>
    );
}