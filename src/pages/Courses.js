import { Fragment } from 'react';
import coursesData from "../data/coursesData";

import CourseCard from '../components/CourseCard';
import { Row } from 'react-bootstrap';


/* 
    the course in our CourseCard component can be sent as a prop
        prop - is a shorthand for "property" since the components are considered as objects in ReactJs
    
    the curly braces {} are used for props to signify that we are providing information using JS expressions rather than hard coded values which use double quotes ""
*/
export default function Courses()
{   
    /* const courses = coursesData.map(course =>
    {
        return (
            <CourseCard key={course.id} courseProp= {course} />
        )
    }) */

    const courses = coursesData.map(course => <CourseCard key={course.id} courseProp= {course} /> );

    return (
        <Fragment>
            <Row>{courses}</Row>
        </Fragment>
    )
}